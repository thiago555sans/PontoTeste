package Test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class MeuControleDePontoTest {
	private LoginPage login;
	private WebDriver driver;
	
	@Before
	public void before() throws IOException{
		System.setProperty("webdriver.chrome.driver","/home/thiago/Downloads/chromedriver");
		driver = new ChromeDriver();
		login= new LoginPage(driver);
		driver.manage().window().maximize();
	//eclipse-workspace/TestePonto/log
	}
	
	@Test
	public void teste(){
		login.navergar();
		login.autenticar();
		login.actions();
		login.logout();
	}
	
	@After
	public void after() throws InterruptedException {
		Thread.sleep(4000);
		driver.quit();
	}
	
}
