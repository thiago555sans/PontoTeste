package Test;



import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public class LoginPage {
	FileLog log=new FileLog();
	String name="thylmt@gmail.com";
	String pass="teste123";
	private WebDriver driver;

	public LoginPage (WebDriver driver) {
		this.driver = driver;
	}

	public void screenShot(String nameTc) {
		// Take screenshot and store as a file format
		File src= ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		try {
			// now copy the  screenshot to desired location using copyFile //method
			FileUtils.copyFile(src, new File("/eclipse-workspace/TestePonto/screnshots" + nameTc + ".png"));
		}
		catch (IOException e)
		{
			System.out.println(e.getMessage());
		}
	} 

	public void navergar() {
		this.driver.get("http://www.meucontroledeponto.com.br/users/sign_in");
		log.escrever("Navegar para a url: http://www.meucontroledeponto.com.br/users/sign_in ", true);
	}

	public void autenticar(){
		try {
			driver.findElement(By.id("user_email")).sendKeys(this.name);
			driver.findElement(By.id("user_password")).sendKeys(this.pass);
			driver.findElement(By.className("bigger-110")).click();
			log.escrever("Autenticação", true);
			screenShot("LoginSucefull");
		} catch (Exception e) {
			log.escrever("Autenticação", true);
			screenShot("LoginUnucefull");
			e.printStackTrace();
		}
	}
	
	public void actions() {
		try {
			driver.findElement(By.xpath("//*[@id=\"sidebar\"]/ul/li[1]/ul/li[2]/a")).click();
			System.out.println("ir para adicionar registro");
			
			driver.findElement(By.xpath("//*[@id=\"day_record_time_records_attributes_0_time\"]")).clear();
			driver.findElement(By.xpath("//*[@id=\"day_record_time_records_attributes_0_time\"]")).sendKeys("1600");
			driver.findElement(By.name("commit")).click();
			System.out.println("adcionando resgistro de entrada manual");
			
			driver.findElement(By.xpath("//*[@id=\"sidebar\"]/ul/li[1]/ul/li[2]/a")).click();
			System.out.println("ir para adicionar registro pela segunda vez");
			
			driver.findElement(By.xpath("//*[@id=\"day_record_reference_date\"]")).click();
			driver.findElement(By.xpath("/html/body/div[3]/div[1]/table/tbody/tr[4]/td[5]")).click();
			System.out.println("selecionando campo de data para justificativa de falta");
			
			driver.findElement(By.xpath("//*[@id=\"new_day_record\"]/div[1]/div[3]/label[2]/div/div/span[3]")).click();
			driver.findElement(By.xpath("//*[@id=\"day_record_reference_date\"]")).click();
			driver.findElement(By.xpath("//*[@id=\"new_day_record\"]/div[1]/div[4]/label[2]/div/div/span[3]")).click();
			driver.findElement(By.xpath("//*[@id=\"day_record_observations\"]")).sendKeys("usahuhusah");
			System.out.println("Confirmando justificativa");
			
			driver.findElement(By.xpath("//*[@id=\"new_day_record\"]/div[2]/div/div[1]/a/span/i")).click();
			driver.findElement(By.xpath("//*[@id=\"new_day_record\"]/input[4]")).click();
			System.out.println("Gerando relatorio");
			
			driver.findElement(By.xpath("//*[@id=\"main-container\"]/div[2]/div[2]/div[2]/h1/a")).click();
			
			
			driver.findElement(By.xpath("//*[@id=\"main-container\"]/div[2]/div[2]/div[3]/div/div[1]/div/div[3]/div/button")).click();
			driver.findElement(By.xpath("//*[@id=\"main-container\"]/div[2]/div[2]/div[3]/div/div[1]/div/div[3]/div/ul/li[1]/a")).click();
			
			screenShot("RelatorioGerated");
		} catch (Exception e) {
			screenShot("RelatorioERROR");
			e.printStackTrace();
		}
		
	}
	
	public void logout() {
		try {
			driver.findElement(By.xpath("//*[@id=\"navbar-container\"]/div[2]/ul/li[2]/a/span")).click();
			driver.findElement(By.xpath("//*[@id=\"navbar-container\"]/div[2]/ul/li[2]/ul/li[3]/a")).click();
			System.out.println("logout");
			screenShot("logoutSucefull");
		} catch (Exception e) {
			screenShot("logoutUnsuceful");
			e.printStackTrace();
		}
	}
}
